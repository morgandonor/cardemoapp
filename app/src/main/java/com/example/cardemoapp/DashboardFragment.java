package com.example.cardemoapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DashboardFragment  extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    View v;




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_dashboard, null);


        ArrayList<serviceHistoryItem> serviceHistoryList = new ArrayList<>();
        serviceHistoryList.add(new serviceHistoryItem(R.drawable.browse, "July 8", "scheduled maintenence"));
        serviceHistoryList.add(new serviceHistoryItem(R.drawable.browse, "July 12", "scheduled maintenence"));
        serviceHistoryList.add(new serviceHistoryItem(R.drawable.browse, "July 16", "scheduled maintenence"));




        mLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new Adapter(serviceHistoryList);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        return v;



    }
}

