package com.example.cardemoapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class creditCardAdapter extends RecyclerView.Adapter<creditCardAdapter.ExampleViewHolder> {

    private ArrayList<CreditCard> mcreditCardArrayList;

    public static class ExampleViewHolder extends RecyclerView.ViewHolder{


        public ImageView mImageView;
        public TextView mTextView1;
        public TextView mTextView2;
        public TextView mTextView3;

        public ExampleViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.ccimage1);
            mTextView1 = itemView.findViewById(R.id.cc1);//Amex ending in 1234, etc
            mTextView2 = itemView.findViewById(R.id.cc2);//Expires ....
            mTextView3 = itemView.findViewById(R.id.chName);//cardholder name
        }
    }

    public creditCardAdapter(ArrayList<CreditCard> creditCardArrayList){

        mcreditCardArrayList = creditCardArrayList;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.credit_cards, parent, false);
        ExampleViewHolder evh = new ExampleViewHolder(v);
        return evh;
    }



    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {

        CreditCard currentItem = mcreditCardArrayList.get(position);

        holder.mImageView.setImageResource(currentItem.getmImageResource());
        holder.mTextView1.setText(currentItem.getmText1());
        holder.mTextView2.setText(currentItem.getmText2());
        holder.mTextView3.setText(currentItem.getmText3());

    }

    @Override
    public int getItemCount() {
        return mcreditCardArrayList.size();
    }
}
