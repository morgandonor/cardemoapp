package com.example.cardemoapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ExampleViewHolder> {

    private ArrayList<serviceHistoryItem> mServiceHistoryList;

    public static class ExampleViewHolder extends RecyclerView.ViewHolder{


        public ImageView mImageView;
        public TextView mTextView1;
        public TextView mTextView2;

        public ExampleViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.cardimage1);
            mTextView1 = itemView.findViewById(R.id.cardtext1);
            mTextView2 = itemView.findViewById(R.id.cardtext2);
        }
    }

    public Adapter(ArrayList<serviceHistoryItem> serviceHistoryItemArrayList){

        mServiceHistoryList = serviceHistoryItemArrayList;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.example_item, parent, false);
        ExampleViewHolder evh = new ExampleViewHolder(v);
        return evh;
    }



    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {

        serviceHistoryItem currentItem = mServiceHistoryList.get(position);

        holder.mImageView.setImageResource(currentItem.getmImageResource());
        holder.mTextView1.setText(currentItem.getmText1());
        holder.mTextView2.setText(currentItem.getmText2());

    }

    @Override
    public int getItemCount() {
        return mServiceHistoryList.size();
    }
}
