package com.example.cardemoapp;

import android.location.Address;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.time.Instant;


public class ProfileFragment  extends Fragment {

    private AppBarConfiguration mAppBarConfiguration;
    private Instant Ion;


    private DrawerLayout mDrawerLayout;
    private FloatingActionButton mFab;

    public static final String MY_TAG = "the_custom_message";

    String JSON_STRING = "{\"name\":\"Welcome back, Henry!\",\"address\":\"6 Welgate Road, Medford MA 02155\",\"phoneNumber\":\"617-771-6749\",\"email\": \"henry@gmail.com\",\"model\":\"2018 Sonata\"}";
    String name;
    String email;
    String model;
    String phoneNumber;
    String address;
    TextView Name;
    TextView Email;//email
    TextView Model;
    TextView PhoneNumber;
    TextView Address;
    View v;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        v = inflater.inflate(R.layout.fragment_profile1, container, false);

         Name = (TextView) v.findViewById(R.id.name);

         Email  = (TextView) v.findViewById(R.id.email);

         Model = (TextView) v.findViewById(R.id.carModel);

         PhoneNumber = (TextView) v.findViewById(R.id.phoneNumber);

        Address = (TextView) v.findViewById(R.id.address);

        try {
            // get JSONObject from JSON file
            JSONObject obj = new JSONObject(JSON_STRING);
            name = obj.getString("name");
            Name.setText(name);
            email = obj.getString("email");
            Email.setText(email);
            model = obj.getString("model");
            Model.setText(model);
            phoneNumber = obj.getString("phoneNumber");
            PhoneNumber.setText( phoneNumber);
            address = obj.getString("address");
            Address.setText(address);

        } catch (JSONException e) {
            //   Log.d(MY_TAG, "TRY CATCH ERROR", e);
            e.printStackTrace();
        }



       //Name = (TextView) v.findViewById(R.id.name);
       // Name.setText("hellpo");
        return v;


    }
}
