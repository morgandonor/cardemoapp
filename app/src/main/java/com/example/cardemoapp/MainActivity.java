package com.example.cardemoapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.time.Instant;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    private Instant Ion;


    private DrawerLayout mDrawerLayout;
    private FloatingActionButton mFab;
    View v;

    /**/
    TextView Name;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //         .setAction("Action", null).show();
                //getMenuInflater().inflate(R.menu.main, menu);

                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                drawer.openDrawer(Gravity.LEFT);


            }


        });





        //to be able to click on tabs and change fragments
        BottomNavigationView bottomnavigationView = findViewById(R.id.bottomNavigationView);
        bottomnavigationView.setOnNavigationItemSelectedListener(this);

        Fragment fragment = new ProfileFragment();
        loadFragment(fragment);


    }


    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();

            return true;
        } else
            return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;

        switch (item.getItemId()) {
            case R.id.profile1:
                fragment = new ProfileFragment();
                break;

            case R.id.browse:
                fragment = new BrowseFragment();
                break;

            case R.id.dashboard:
                fragment = new DashboardFragment();
                break;

            case R.id.services:
                fragment = new ServicesFragment();
                break;

            case R.id.payments:
                fragment = new PaymentsFragment();
                break;

            /*case R.id.drawer_layout:
                fragment = new Drawer();*/

        }

        return loadFragment(fragment);
    }



}