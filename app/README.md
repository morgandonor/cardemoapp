# CAR DEMO APPLICATION

My first attempt in app devlopment, making a demo car app that demonstrates the Vee24 SDK

## Installation

Download the files, made in Android Studio.

```bash
git clone https://morgandonor@bitbucket.org/morgandonor/cardemoapp.git
```


## Contributing
Still working on this app occasionally over the course of the school year. CONTACT INFO:

EMAIL: zmorgan18@icloud.com

PHONE: 617-599-6183

## Next Steps
Spreadsheet for next steps can be found  [here](https://docs.google.com/spreadsheets/d/1Bf3a45NAu75_H2tUSaU593VF8apgWRQclz-zaJqWEbQ/edit#gid=627450661)

## License
[ZACH MORGAN](https://storm.cis.fordham.edu/~zmorgan3/)